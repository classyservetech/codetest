# Code Test


## Run the project

Activate the virtual environment first

```bash

cd <project path>
pip install -r requirements.txt
python manage.py runserver
```

## Run celery in another terminal

```bash
cd <project path>
celery -A votingapp worker -l info
```
