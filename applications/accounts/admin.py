from django.contrib import admin
from django.contrib.auth import get_user_model
from django.utils.safestring import mark_safe
from django.conf import settings
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import UserProfile



class UserAdmin(BaseUserAdmin):
    search_fields = ['email', 'username', 'last_name']
    list_display = ['username', 'user_image', 'last_name', 'email', 'is_client_user', 'date_of_birth']
    list_filter = ['username', 'last_name', 'email', 'is_client_user']
    list_editable = ['is_client_user',]

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal info', {'fields': ('first_name','last_name','email','date_of_birth', 'is_client_user', 'profile_image')}),
        ('Permissions', {'fields': ('is_active','is_staff','is_superuser','groups','user_permissions' ,)}),
	    ('Important dates', {'fields': ('last_login','date_joined',)}),
    )
    
    def user_image(self, obj):
        return mark_safe('<img src="{url}" width="{width}" height={height} style="border-radius: 50%;"/>'.format(
            url=obj.profile_image.url if obj.profile_image else settings.STATIC_URL + 'img/default.png',
            width=40,
            height=40,
        )
        )


admin.site.register(UserProfile, UserAdmin)