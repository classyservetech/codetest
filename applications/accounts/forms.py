
import re
from django import forms
from django.contrib.auth import authenticate
from django.conf import settings

from applications.accounts.models import UserProfile


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.EmailInput(), required=True)
    password = forms.CharField(widget=forms.PasswordInput(), required=True)

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'placeholder': 'Email *'})
        self.fields['password'].widget.attrs.update({'placeholder': 'Password *'})

    def clean_username(self):
        if not self.cleaned_data.get('username'):
            raise forms.ValidationError("Username required")
        data = self.cleaned_data.get('username')
        if not UserProfile.objects.filter(username=data):
            raise forms.ValidationError("Username does not match")
        return data

    def clean_password(self):
        if not self.cleaned_data.get('password'):
            raise forms.ValidationError("password required")
        return self.cleaned_data.get('password')

    def clean(self):
        if self.cleaned_data.get('username') and self.cleaned_data.get('password'):
            user = authenticate(username=self.cleaned_data.get('username'), password=self.cleaned_data.get('password'))
            if not user:
                raise forms.ValidationError("invalid credentials")
        return self.cleaned_data


class SignUpForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(), required=True, min_length=6)
    password2 = forms.CharField(widget=forms.PasswordInput(), required=True, min_length=6)
    date_of_birth = forms.DateField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.CharField(widget=forms.EmailInput(), required=True)

    class Meta:
        model = UserProfile
        fields = ('username', 'first_name','last_name', 'email', 'date_of_birth', 'password', 'password2')

    def clean_email(self):
        email = self.cleaned_data['email']
        if UserProfile.objects.filter(email=email):
            raise forms.ValidationError("Email already exist")
        return email

    def clean_username(self):
        username = self.cleaned_data['username']
        if UserProfile.objects.filter(username=username):
            raise forms.ValidationError("Username already exist")
        return username

    def clean(self):
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')
        if password != password2:
            raise forms.ValidationError("passwords do not match")


class AddClientForm(forms.ModelForm):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    profile_image = forms.ImageField(required=True)
    email = forms.EmailField(required=True)
    date_of_birth = forms.DateField(required=True)

    class Meta:
        model = UserProfile
        fields = ('username', 'first_name','last_name', 'email', 'date_of_birth', 'profile_image')
