from datetime import date

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext as _


class TimeStampModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class UserProfile(AbstractUser, TimeStampModel):
    profile_image = models.ImageField(null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    is_client_user = models.BooleanField(default=False)

    @property
    def get_age(self):
        today = date.today()
        dob = self.date_of_birth
        if dob:
            age = today.year - dob.year - ((today.month, today.day) < (dob.month, dob.day))
            return age if age > 0 else 0
        return 0

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
