import os
from datetime import date
from datetime import datetime
from datetime import timedelta
from openpyxl import Workbook
from django.http import HttpResponse

from celery import shared_task
from applications.accounts.models import UserProfile


def get_age(dob):
    today = date.today()
    if dob:
        age = today.year - dob.year - ((today.month, today.day) < (dob.month, dob.day))
        return age
    return 0


@shared_task
def download_xlsx():
    """
        Downloads all users as Excel file with a single worksheet
        """
    user_queryset = UserProfile.objects.all()

    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    )
    response['Content-Disposition'] = 'attachment; filename={date}-users.xlsx'.format(
        date=datetime.now().strftime('%Y-%m-%d'),
    )
    workbook = Workbook()

    # Get active worksheet/tab
    worksheet = workbook.active
    worksheet.title = 'Movies'

    # Define the titles for columns
    columns = [

        'Username',
        'Name',
        'Sur Name',
        'email',
        'Date of birth',
        'Age',

    ]
    row_num = 1

    # Assign the titles for each cell of the header
    for col_num, column_title in enumerate(columns, 1):
        cell = worksheet.cell(row=row_num, column=col_num)
        cell.value = column_title

    # Iterate through all movies
    for user in user_queryset:
        row_num += 1
        # Define the data for each cell in the row
        row = [
            user.username,
            user.first_name,
            user.last_name,
            user.email,
            user.date_of_birth,
            get_age(user.date_of_birth),
        ]

        # Assign the data for each cell of the row
        for col_num, cell_value in enumerate(row, 1):
            cell = worksheet.cell(row=row_num, column=col_num)
            cell.value = cell_value

    workbook.save('users-list.xlsx')
    return True
