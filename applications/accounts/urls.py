#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views
from django.contrib.auth import logout
from django.conf import settings
from django.contrib.auth.decorators import login_required


urlpatterns = [
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^signup/$', views.SignUpView.as_view(), name='signup'),
    url(r'^add-client/$', views.AddClientUser.as_view(), name='add-client'),
    url(r'^voting/$', views.VotingView.as_view(), name='voting'),
    url(r'^delete/$', views.DeleteView.as_view(), name='delete'),
    url(r'^download/$', views.DownloadView.as_view(), name='download'),
    url(r'^$', login_required(views.UsersListView.as_view()), name='users-list'),
]
