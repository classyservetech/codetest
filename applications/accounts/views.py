import os
from time import sleep

from django.views import generic
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.http import JsonResponse
from django.conf import settings
from django.http import HttpResponse

from applications.accounts.models import UserProfile
from applications.accounts.forms import LoginForm, SignUpForm, AddClientForm
from applications.accounts.tasks import download_xlsx


class SignUpView(generic.FormView):
    template_name = 'signup.html'
    form_class = SignUpForm

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        user = form.save(commit=False)
        user.set_password(form.cleaned_data["password"])
        user.save()
        login(self.request, user)
        return HttpResponseRedirect('/')

    def form_invalid(self, form):
        print(form.errors)
        return self.render_to_response(self.get_context_data(form=form))


class LoginView(generic.FormView):
    template_name = 'login.html'
    form_class = LoginForm

    def form_valid(self, form):
        user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
        login(self.request, user)
        return HttpResponseRedirect('/')

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))


class VotingView(generic.ListView):
    template_name = 'voting.html'
    model = UserProfile
    context_object_name = 'users'
    queryset = UserProfile.objects.filter(is_client_user=True)


class UsersListView(generic.ListView):
    template_name = 'users_list.html'
    model = UserProfile
    context_object_name = 'users'
    queryset = UserProfile.objects.filter(is_superuser=False, is_client_user=True)


class DeleteView(generic.View):

    def get(self, request):
        if self.request.is_ajax():
            user_id = request.GET.get('id')
            UserProfile.objects.get(id=user_id).delete()
            return JsonResponse({'status': 'success'})


class DownloadView(generic.View):

    def get(self, request):
        result = download_xlsx.apply_async()
        while not result.ready():
            sleep(5)
        file_path = os.path.join(settings.BASE_DIR, 'users-list.xlsx')
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
                return response


class AddClientUser(generic.FormView):
    template_name = 'add_client.html'
    form_class = AddClientForm

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_client_user = True
        user.save()
        return HttpResponseRedirect('/')

    def form_invalid(self, form):
        print(form.errors)
        return self.render_to_response(self.get_context_data(form=form))
