from django.contrib import admin

from applications.vote.models import Vote


admin.site.register(Vote, admin.ModelAdmin)
