from django.db import models
from django.utils.translation import ugettext as _

from applications.accounts.models import UserProfile


class Vote(models.Model):
    candidate = models.ForeignKey(UserProfile, related_name='candidate', on_delete=models.CASCADE)
    voters = models.ManyToManyField(UserProfile, related_name='voters')

    def __str__(self):
        return self.candidate.username

    class Meta:
        verbose_name = _('Vote')
        verbose_name_plural = _('Votes')
