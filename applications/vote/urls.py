#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views
from django.contrib.auth import logout
from django.conf import settings
from django.contrib.auth.decorators import login_required


urlpatterns = [
    url(r'^$', views.VoteView.as_view(), name='vote'),
]
