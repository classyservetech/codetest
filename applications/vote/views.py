from django.shortcuts import render
from django.views import generic
from django.http import JsonResponse

from applications.accounts.models import UserProfile
from applications.vote.models import Vote


class VoteView(generic.View):

    def get(self, request):
        if self.request.is_ajax():
            candidate_id = request.GET.get('id')
            candidate = UserProfile.objects.get(id=candidate_id)
            vote_obj = Vote.objects.get_or_create(candidate=candidate)
            voted_list = list(vote_obj[0].voters.all().values_list('id', flat=True))
            if self.request.user.id in voted_list:
                vote_obj[0].voters.remove(self.request.user)
                action = 'Un voted'
            else:
                vote_obj[0].voters.add(self.request.user)
                action = 'Voted'
            vote_obj[0].save()
            count = vote_obj[0].voters.all().count()
            return JsonResponse({'status': 'success', 'count': count, 'action': action})
