import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'votingapp.settings')

# app = Celery('votingapp')
app = Celery('votingapp', backend='rpc://', broker='pyamqp://')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
